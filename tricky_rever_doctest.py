def _get_tricky_reversed_word(word: str) -> str:
    """Get reversed word (alpha symbols - revers, not-alpha - stay on old places).

    Usage examples:
    >>> _get_tricky_reversed_word('asdf')
    'fdsa'
    >>> _get_tricky_reversed_word('')
    ''
    >>> _get_tricky_reversed_word(3)
    Traceback (most recent call last):
    ...
    TypeError: unsupported type <class 'int'>. Expected 'str'.
    """
    if not isinstance(word, str):
        raise TypeError(f"unsupported type {type(word)}. Expected 'str'.")
    inverted_word = ""
    letters = [letter for letter in word if letter.isalpha()]
    for symbol in word:
        inverted_word += letters.pop() if symbol.isalpha() else symbol
    return inverted_word


def tricky_revers(text: str) -> str:
    """Get reversed text (alpha symbols - revers, not-alpha - stay on old places).

    Usage examples:
    >>> tricky_revers('asdf')
    'fdsa'
    >>> tricky_revers('')
    ''
    >>> tricky_revers(3)
    Traceback (most recent call last):
    ...
    TypeError: unsupported type <class 'int'>. Expected 'str'.
    """
    if not isinstance(text, str):
        raise TypeError(f"unsupported type {type(text)}. Expected 'str'.")
    words = text.split()
    reversed_words = map(_get_tricky_reversed_word, words)
    return " ".join(reversed_words)


if __name__ == "__main__":
    import doctest
    doctest.testmod()